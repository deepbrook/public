# Cells

This is the page on which the different cells within OpenCraft are described, and where cell-specific
rules are documented.

## Serenity

Internal tools:

* [Chat](https://chat.opencraft.com/opencraft/channels/serenity)
* [Weekly sprint board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=24)
* [Backlog](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=24&view=planning)
* [Epics board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=27)

Members:

* Adolfo R. Brandes
* Aleksandr Sobolev
* Daniel Clemente Laboreo
* Fox Piacenti
* Gábor Boros
* Geoffrey Lehée
* Jill Vogel
* Kahlil Hodgson
* Matjaz Gregoric
* Nizar Mahmoud
* Pooja Kulkarni
* Raul Gallegos
* Samuel Walladge
* Sid Verma
* Tim Krones
* Usman Khalid

Roles:

* [Recruitment manager](roles.md#cell-manager-recruitment): Usman Khalid
* [Sprint manager](roles.md#cell-manager-sprint-management): Matjaz Gregoric
* [Epic manager](roles.md#cell-manager-epic-planning): Tim Krones
* [Sustainability manager](roles.md#cell-manager-sustainability-of-the-cell): Daniel Clemente Laboreo
* [Prioritization manager](roles.md#cell-manager-prioritization-of-work): Jill Vogel
* [OSPR Liaison](roles.md#ospr-liaison): Jill Vogel
* [Official forum moderator](roles.md#official-forum-moderator): Samuel Walladge
* [DevOps Specialist](roles.md#devops-specialist): Adolfo R. Brandes

Cell-specific rules:

* None

## Bebop

Internal tools:

* [Chat](https://chat.opencraft.com/opencraft/channels/bebop)
* [Weekly sprint board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=26)
* [Backlog](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=26&view=planning)
* [Epics board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=28)

Members:

* Aayush Agrawal
* Demid Avramenko
* Farhaan Bukhsh
* Giovanni Cimolin da Silva
* Guruprasad Lakshmi Narayanan
* Josh McLaughlin
* Kshitij Sobti
* Patrick Cockwell
* Paulo Viadanna
* Piotr Surowiec
* Shimul Chowdhury

Roles:

* [Recruitment manager](roles.md#cell-manager-recruitment): Paulo Viadanna
* [Sprint manager](roles.md#cell-manager-sprint-management): Giovanni Cimolin da Silva
* [Epic manager](roles.md#cell-manager-epic-planning): Kshitij Sobti
* [Sustainability manager](roles.md#cell-manager-sustainability-of-the-cell): Guruprasad Lakshmi Narayanan
* [OSPR Liaison](roles.md#ospr-liaison): Giovanni Cimolin da Silva
* [Official forum moderator](roles.md#official-forum-moderator): Paulo Viadanna
* [DevOps Specialist](roles.md#devops-specialist): Giovanni Cimolin da Silva

Cell-specific rules:

* None

## Outside of cells

* Braden MacDonald
* Gabriel D'Amours
* Xavier Antoviaque

Roles:

* [Official forum moderator](roles.md#official-forum-moderator): Xavier Antoviaque
