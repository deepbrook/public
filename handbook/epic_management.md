# Epic ownership

Just like a regular ticket, an [epic] has both an assignee (which we refer to as the [epic owner]) and a reviewer (which we refer to as the epic reviewer).

Concrete expectations that we have for epic owners are laid out in the [epic owner] role of this handbook. To get a refresher on these expectations, go ahead and have another look at that section now.

The main role of the epic reviewer is to take over the responsibilities of the epic owner while they are away, as well as any additional tasks and responsibilities that are specific to a given epic. It is the responsibility of the epic owner to coordinate their time off with their epic reviewer.

Also keep in mind that if you do the [discovery] for a future epic, you will be expected to take on the epic owner role for that epic later on (if/when the client gives us the go-ahead for starting the work).

## Tips and tricks

Here are some tips/tricks/strategies concerning different aspects of epic ownership that should be helpful to you going forward.

Be mindful about the number of epics you take on as an epic owner. If you're new to epic ownership, it's best to focus on a single epic; and even if you have some experience managing epics, it's better to keep the total number of epics that you own fairly small. Otherwise you'll need to do a lot of context-switching during the day/week/sprint, which tends to affect productivity and can even become frustrating at times. Also, if you own too many epics it can be hard to devote enough time to managing each one and being proactive where you should be. As a rule of thumb, aim to own one or two epics at most.

## Discovery phase

Below is a list of points to be aware of when performing a [discovery] for a new project/client.

* Before writing the [discovery], get clarity on the level of detail that is required. If the ticket description doesn't say, ask the person that created the ticket whether you should do a high-level discovery with [ballpark estimates], or a more detailed discovery with stories and detailed estimates. Make sure to read the entire documentation on [how to do estimates] at least once, and reference relevant parts of it while doing the discovery.
* Most discoveries are [timeboxed], so plan your time carefully against the requirements of the discovery. If you don't have enough time to go into detail on a particular part of the estimation, either designate it as something that will require further investigation, or give a quick ballpark estimation range.
* Think about whether or not it would make sense to set up a weekly stand-up meeting with the client for the duration of the project. This is especially important if multiple teams are involved in the project.
   * The purpose of that meeting would be to provide status updates to the client, get status updates from other teams that are involved in the project, address any open questions, and make sure all parties are unblocked and clear on the next steps.
   * It should be enough to allocate 30-45m per meeting.
   * It's important to determine whether this is something that the client would want prior to the implementation phase because we will need to account for it in the [discovery] document and quote that we send to the client. A budget of 1-2h per week for meetings should work well in most cases. (Note that it can't just be 30m because you'll also need a little bit of time to prepare for/follow up on each meeting.)
   * Don't schedule the weekly meeting (or any other meeting that you might need to set up over the course of the project) for right before your EOD. To make sure you'll have time to list out (or directly follow-up on) any action items and add your notes from the meeting to existing tickets as necessary, the meeting should end no later than 30m to 1h before the time you normally stop working.
* Discuss the hours requirement for the project with the [epic manager] and Braden and/or Xavier, so they can determine whether we have capacity within the team for the project, or need to hire to accommodate.
* Note that the number of hours that OpenCraft can allocate to the project per month might differ month-to-month. If that's the case you'll need to keep it in mind when planning out different phases of the project, and when scheduling stories for specific sprints.
* If the project has a deadline built-in (such as the start of a course that is supposed to be using a set of features to develop as part of the project), the number of hours that OpenCraft can allocate to the project will affect the scope of the project. (More specifically, it will be an upper bound for the amount of work that we can schedule and complete prior to the deadline.)
* If the project does not have a deadline built-in, the number of hours that OpenCraft can allocate to the project will affect the duration of the project.
* Leave time for a QA phase after delivering the features laid out in the [discovery] document. This is especially important for projects that are about developing features that a client wants to use in an upcoming course. Ideally, you should allow 2-3 sprints for QA for projects with specific deadlines, but 1 sprint is the absolute minimum. If you only have 1 sprint for QA, make sure that the client delivers their feedback within the first half of the first week of that sprint. Otherwise it will be very tricky to address their feedback, deploy changes, and give the client enough time for another round of testing before the final deadline (which might coincide with the start of a course that depends on the features developed over the course of the project).
* If your epic has any dependencies on edX (product approval of the plan, PR reviews, etc.) then leave lots of time for those to happen and have a contingency plan for what to do if edX delays things too long. Also, try to get specific people from edX to be aware of the project and its timeline, and to commit to being available for product and technical reviews.
* Keep in mind that OpenCraft has a [policy to upstream as much work as possible]. That aspect can have a major impact on the budget and timeline of a project and should therefore be taken into account right from the start. In some cases it might be necessary to reduce the scope of the changes to include in an epic, to make sure that the client's budget will also cover the costs for upstreaming those changes.

## Implementation phase

Once the discovery and estimates are accepted by the client, the project enters the implementation phase.

* To help everyone (including yourself) get a sense of where a project is at, add a timeline to the epic description.
    * In the beginning stages of a project, the timeline can be fairly high-level. For example, it could just list the main phases of the project:
        * Jan 2019: Discovery phase
        * Feb 2019 -- April 2019: Implementation phase
        * May 2019: Deployment phase
        * June 2019: QA
        * etc.

* Note that for future reference it's helpful to mention all phases of the project, even ones that are already in the past at the time you create the timeline.
* At any given stage of the project, the timeline for the next two sprints should be laid out in detail: Add the name of each sprint to the project phase that it belongs to, mentioning when it ends, and list the stories that are scheduled for it. Once all of the stories belonging to a given sprint are done, put a checkmark next to the name of the sprint. A good point in time to make these updates is towards the end of a sprint, when working on the epic update.

    This format provides a concise overview of the project's status to anyone that might be looking at your epic.

* If the scope/plan for the project changes significantly, try to keep the epic description field in Jira updated, so that it always has a useful and accurate explanation of the project, plan, timeline, etc.
* Look at epic updates as a service to yourself, not just to the person who is supposed to eventually read it, whoever that may be. It allows you a dedicated time to consolidate what is going on with the epic, to do any higher-level planning work, and to ask epic-level questions to your peers in the context of the update. It's one of the duties that at first may look to be a chore you just have to do as epic owner, but you and your epic can benefit tremendously from it just through a change in perspective. Spend the next epic update trying to really drill a little deeper, and you'd be surprised how much more of a handle you feel you have on it.
* If you are planning to go on vacation at some point over the course of the project, make sure to coordinate with your epic reviewer a few weeks ahead of time. If the project includes weekly stand-ups, your epic reviewer will need to be able to fill in for you at these meetings. This means that they will need to be familiar with the structure of the meetings, and that while you're away the meetings will need to happen at a time that is not in the middle of the night for them. So if necessary, make sure to move the meeting to a different time before you go on vacation. Let the client know when you will be away, and who will be filling in for you. Also, have the epic reviewer attend the last one or two meetings before you leave so they can meet the client and see first-hand what the structure of the meetings is like.

## Day to day

At the start of your day, get an overview of all updates related to your epic. (You can log time spent on this on the epic itself.)

* Create tasks for any fires that might have come up and coordinate with the rest of the team to make sure they can be addressed right away.

   * In most cases your first point of contact should be the epic reviewer and/or other team members that already have context on the project. If they don't have time to address the fire, the next step will be to involve the sprint firefighters to see if they can help free up some time for the epic reviewer and/or other team members that already have context on the project and should ideally be working on the fire.
    * Don't forget to notify the client that we are looking into whatever issue they reported.
* Determine priority for the remaining updates, relate them to existing tickets, and decide when to work on them.

At the end of this process you should be fully up-to-speed on the project's status, and it should be clear to you what aspects of the project (tasks, reviews, client communication) you'll need to work on that day. It should also help minimize interruptions throughout the rest of the day, allowing you to focus on completing the work that's on your agenda for the day.

## Client communication

Guidelines to help communication with clients:

* When a client reaches out to you (via e-mail, Trello, JIRA, etc.), don't feel compelled to reply right away. Instead, take a moment to determine
    * how important their request is relative to other work from the project that is currently in flight,
    * whether the current sprint includes a task that the request is related to, and
    * what the best way of addressing the request would be.
* We have a [policy to respond to client requests within 24h], so unless you're dealing with something that needs to be addressed right away (such as a client's instance being down), it's perfectly fine to take some time to figure out the best way forward.
* Make sure to thank the client for any feedback that you get from them, even if it's negative. If necessary, apologize and admit mistakes, but mainly focus on finding a solution, and involve the client in the process of coming up with a solution as much as possible.
* Tailor the level of detail to your client and their interests. For example, if you're speaking to an operations specialist, it's ok to go into technical details about a change or question. But if you're speaking to a site manager, they may only want to know the benefits and risks of a change, not the technical details.
* If you need to receive sensitive information from a client (e.g. a SSO client ID and secret), ask them to use [onetimesecret.com](https://onetimesecret.com), and to send each part of the secure information using separate OTS links. Reference our [security policy] for more details.

[epic]: glossary.md#epic
[discovery]: glossary.md#discovery
[epic owner]: roles.md#epic-owner
[epic manager]: roles.md#cell-manager-epic-planning
[ballpark estimates]: how_to_do_estimates.md#how-to-do-a-ballpark-estimate
[how to do estimates]: how_to_do_estimates.md
[timeboxed]: glossary.md#timebox
[security policy]: security_policy.md#sending-sensitive-information-to-clients
[policy to respond to client requests within 24h]: roles.md#communication
[policy to upstream as much work as possible]: sustainability_policy.md#objectives
