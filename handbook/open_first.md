# Open first

OpenCraft’s values, culture and processes are heavily influenced by open source, and openness in particular is an important topic for OpenCraft - it is the [first of our values](values.md). One of the golden rules of open source project management is to [avoid private discussions](https://producingoss.com/en/producingoss.html#avoid-private-discussions). It is an important element to ensure that openness and transparency are more than just words in a company statement - this is what potentially allows anyone to know what is happening in the areas one is interested in. The fact that every discussion and work is done in the open in the first place ensures there are no gatekeepers with the power to conceal or filter information that is inconvenient to them.

And the very act of discussing in public helps ensure that the larger group is being considered in the discussion - indeed, if others can potentially read the conversation, there is a strong disincentive to be dismissive of people who might read it, or to make a move that will be detrimental to them. It helps us to remember to be more inclusive, in every daily interaction - and this is a key element to reduce politics, clique and in-fighting, so prevalent in so many organizations (though it isn’t sufficient by itself). Just like, with the right circumstances, writing code in the open can push us to write better code, discussing in the open can push us to have better discussions.

It also can help to find information about something in the future - most of the tools we use are searcheable, so even months after it allows to search the chat or ticket tracker to learn more about something, even when the all people involved are away.

Of course, there is a drawback to this - if more people can read and participate, discussions can take more time. It can also require more efforts to explain things to someone who is not familiar with the topic or its history, or to consider how others might take something. In a private discussion, you don’t even have to think about this.

This isn’t just a drawback though - it’s also a feature. If it takes time to explain and discuss things, it’s often because the discussion or explanations are needed - most people wouldn’t be spending time discussing something if that wasn’t something important or useful to them. And the time spent considering others, who would otherwise have been excluded from that discussion, is repaid by the strengthening effect it has on the organization and how it empowers everyone. It helps avoiding having to do things “just because” - there might still be things we do that one disagrees with, but there is always a way to figure out why we do things a certain way and get the full context, not just the grapevine version of it.

Of course, there are exceptions - this isn’t completely absolute. For example, we currently exclude newcomers reviews, financials and 121s from public discussions. Some elements surface with more difficulty in public, so having some private spaces help ensuring we don’t end up ignoring something important because of it. But generally the rule of thumb is “open first”, i.e. to default to openness unless there is a very good reason not to. And to be clear, gaining time, being easier or more comfortable aren’t good reasons by themselves to not have a conversation in the open. Just like they wouldn’t be good reasons to produce proprietary code - or simply poor quality code. The additional effort and time we put into producing quality work includes our communications - it’s also something that deserves to be well crafted.

## What about small discussions nobody cares about?

One common area where many people hesitate is the “small things”. For example, being late for a meeting - do I ping that person in a public or private channel? It seem to be of little value to anyone but that person, so it seem better to avoid spamming a public channel with this, right? 

The thing is, no matter how small something looks to you, you can never tell what will be its importance for someone else. Maybe someone else will have a question about that meeting. Or there will be something in the discussion that it will generate that will be interesting for someone else. Sometimes it will only be interesting to you, but you can’t know in advance, and you can’t know what will interest others. And once a discussion has started in private, it takes more effort to move it to a public channel than starting it there in the first place – again, like open source. 

Defaulting to open makes it a no-brainer - something you take as a habit - and even if it sometimes takes some getting used to at the beginning, as transparency can be intimidating, it can become very natural and obvious.

From Jill: _"I compare the discomfort of “always talking openly” to my initial horror at having all my PR reviews open to anyone who wanted to see them. But doing it all the time got me over the discomfort, and better at the code and discussions. I don’t agonize nearly as much over client communications now either… and if I’m uncertain, I post it for review first."_

## That's a lot to follow!

If you try to follow all the conversations happening, you will quickly overload - don't. You don’t need to, if it’s explicitly random to you. As long as you answer all explicit pings and the [announcements forum](https://forum.opencraft.com/c/announcements/7) within 24h Mon-Fri, and review the rest of the discussions forum once or twice a week to see if there is anything you should answer, then the rest is when you think it’s important to follow.

But if in the future you are looking for something, and search the chat or the ticket tracker, that conversation will come up, and at that point it will no longer be random. You can also more easily log the time in the ticket that caused you to look it up.

## Current limitations & expanding the public field

While most of our conversations are public within the team, currently a large majority of them are actually private to anyone not in the team. We do have many fully public conversations - like our reviews on PRs, discussions on public edX Jira tickets, or our handbook PRs & reviews. But very few clients are willing to work fully in public, and we have to be realistic about that constraint, so the discussions related to clients are only public to team members. We would like to push more discussions to the open though, and try to encourage the third parties we interact with to do the same.
