# Process for sprints

Please find below the description of the process for sprints. While it might
sound a bit convoluted, it's pretty simple in practice. We work in two-week
sprints. Each cell has its own board, backlog and sprint, but they are all held
in sync.

| Day of Sprint     |   |
|-------------------|----------------------------------------------------------|
| Day 0 (Monday)    | Each cell holds a [sprint planning meeting](sprint_planning_agenda.md) to do a final review of the plans for the upcoming sprint, and start the sprint. We use the [Sprints app] to plan our team's time (that everyone has enough work and nobody will be overcommitted) and to collectively make sure we're staying on track with each client's monthly budget and each epic/project budget. |
| Day 0 or 1        | Each developer should review every ticket they have in the sprint, to confirm the due date, plan when/how to approach the work, coordinate with the reviewer for time-sensitive reviews, etc. |
| Days 0-4          | Development - each team member works on their tickets and code reviews. Tasks move from left to right until completion on our JIRA tracker. |
| Day 7 (Monday)    | Each cell holds a [mid-sprint meeting](sprint_planning_agenda.md) to ensure everything is going smoothly, and discuss how to help with tasks that are at risk of not being finished by the end of the sprint. |
| Days 7-11         | Development continues |
| Day 10 (Thursday) | Each epic owner posts an epic update, which includes creating and prioritizing all tasks from that epic that their cell should work on in the upcoming sprint. |
| Day 11 (Friday)   | Cells collaboratively and asynchronously refine and estimate tasks complexity. Once done, they assign tasks and reviews to themselves.<br> **End of day Friday is the deadline for creating and assigning stories to the upcoming sprint**, and is also when the asynchronous refinement session is closed. |
| Day 14 (Monday)   | Depending on their timezone, some developers may have some time on Monday to finish up development or work ahead before the new sprint starts. |

## Detailed version

### All Roles

* In general, "work from the right" - it's better to finish a task that's already in progress
  than it is to start a whole new task. This reduces context switching, which will help get
  things done more quickly. With that said, if you are blocked on a task, move on to another
  task, until unblocked.
* Daily, use the Tempo time tracking on the Jira board to record the time spent on each task
  and update each ticket being worked on once finished on it for the day. Even just a quick
  summary or sentence of where you're at with the task is useful to your reviewer.

### Developer Team

* All tasks are split up into epics, and each epic has an "epic owner."
* Near the end of each sprint, in preparation for the next one, the team
  will do an asynchronous "refinement" session. This means that we each
  estimate the complexity of each task in the upcoming sprint in terms
  of "story points". The person in the cell responsible for the sprint planning is the one who
  starts this refinement session and invites everyone to it.
    * We use story points as a shorthand for agreeing about the complexity of a task.
      Story points are meant to be a relatively objective measure of "complexity",
      and not necessarily indicate how long it would take any particular developer to do.
    * We use a fibonacci scale for story points (please see
      [task workflows](task_workflows.md) for more detail on story points for different types
       of tasks).
    * If anything about the task or the acceptance criteria is unclear,
      post a question as a comment on the task and try to work with the epic owner
      (or the reporter if there is no epic owner) to get it answered before the meeting.
      Note that during the asynchronous refinement sessions, any comments/questions
      posted in the estimation session will be lost once the refinement session ends,
      so it's preferable to post questions on the tickets themselves.
* Likewise, before the Monday planning meeting, everyone works
  to asynchronously assign each story in the upcoming sprint to a developer
  and a code reviewer.
    * The epic owner is responsible for making sure this happens for each story
      in their epics that must happen in the upcoming sprint. For stories that
      don't have an epic owner, either the tentative assignee (if there is one
      already) or the person who reported the ticket is responsible for doing
      this; this is also documented
      [in our Jira bot Crafty's code](https://gitlab.com/opencraft/dev/jira-scripts/blob/bf4ea0ae3447fc2ae2098444c2b13a7d2f9e06c3/ReadyForSprintReminder.groovy#L112-124)
    * Epic owners should also check the
      [Sprints app]
      for the upcoming sprint, ensuring that the tasks the team plans to take on
      for the upcoming sprint are in line with each client's budget and each
      epic's budget.
* We have a meeting on Monday via video chat. There we review the refinement and assignments
  for the upcoming sprint (please see the [sprint planning agenda](sprint_planning_agenda.md)
  for details of this meeting).
    * The meeting link is in the calendar invite.
    * If you don't see the meetings as recurring events on your calendar, ask Xavier to send
      you the invitation.
* After the sprint has been planned and started, it's time to code! If it's your first sprint, your
  mentor should have assigned you a task, in addition to the onboarding task.

During the sprint:

* Take a look at the [current sprint board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=5).
  There are filters at the top such as <em>My Issues</em>, <em>My code reviews</em>, and your
  name, which can be toggled to show only issues relevant to you.
* Drag a task from column to column on the sprint board to update its status as you work through it.
  The various statuses/columns are described below. Tasks move from left to right until completion.
* In general, "work from the right" - it's better to finish a task that's already in progress than
  it is to start a whole new task. This reduces context switching, which will help get things done
  more quickly; it also demonstrates reactivity to upstream reviewers (which pushes them to be
  reactive too).
* Use the Tempo timekeeping system built into our JIRA board for tracking the time you spend on
  each task.

[Sprints app]: https://sprints.opencraft.com
