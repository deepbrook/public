# Before the meeting

## Everyone in the cell

### If it is the beginning of a new sprint

Work through the [End of Sprint
Checklist](https://docs.google.com/forms/d/e/1FAIpQLSejhZ-TaAy8G5eD9UUz4YC1kB57hKO310IEUmhWFAGrqE1LdA/viewform).
This will be sent out in a reminder towards the end of each sprint. It includes
the points listed below to work through, and a short questionaire about how you
felt during the sprint. Note that the points below are canonical; the checklist
will only include a brief listing of the points.

* Do an [epic update](roles.md#epic-owner) for each epic you own.
* Review/adjust the "Time Remaining" estimate for every story of yours still in
  the sprint. Check the [sprint planning
  dashboard](https://sprints.opencraft.com/) for any tickets in the "Unestimated"
  column and set a remaining estimate for them.
* Review all your stories in "External Review/blocker" and move them to "Long
  External Review/Blocked" if you don't expect/want to work on them in the
  coming sprint.
* Review all your stories in "Long External Review/Blocked", and: if they
  haven't moved in a while, ping someone about it (e.g. ping edX to review),
  and if they are now unblocked, move them into the upcoming sprint.
* If you have any task that will [spill over](time_management.md#spillovers), add a comment
  explaining the spillover reason to the ticket following this template:
  `[~crafty]: <spillover>COMMENTS HERE</spillover>`.  Also ensure that these
  spillover tickets have a comment clarifying their status, a new deadline up
  to the end of the week, an accurate "Remaining Estimate".
* Participate in the [asynchronous refinement session](#refinement-session) to help
  refine the team's stories for the upcoming sprint.
* Assign stories and reviews from the upcoming sprint to yourself, and set the
  "Remaining" estimate on each one
* Check that the "Committed" and "Goal" columns on the sprint planning
  dashboard match your expectations, and that you have committed all your hours
  for the upcoming sprint without overcommitting.
* Add a short description of anything that went well and anything to improve
  from the sprint to the sprint retrospective spreadsheet.

**For the sprint managers only**

* Create and assign firefighting & discovery duty tickets for next sprint
* Review all the "Deployed & Delivered" stores and move them to "Done"
* Do a quick pass over the upcoming sprint's backlog, and move big/important stories to the top
* Remind Firefighter 1 to help epic owners get all tickets green before meeting

## Refinement session

The Sprint Manager create the sprint refinement session in JIRA after the epic owners put tasks into the upcoming sprint for their projects.
An email with a link is sent out when the session opens. For each task:

* Read through the description
* Decide whether you can take it
* If you do want it, it's ok to spend a bit more time investigating
* If you have questions, put them in comments on the ticket
* Assign points as described in the [process section](process.md)
* Use the "estimate comments" box to qualify estimates
* Choose `?` if you have no idea how many points
* A little green `(/)` will appear next to your picture up top when you are done

Complete the estimation session before Monday, if possible, or before the meeting at the latest. Stay conscious of time budgets and try not to log more than a few minutes per task to do these estimates.

## Meeting lead

* Confirm [who is scheduled to lead the meeting](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit), and who is writing down minutes in the [minutes document](https://docs.google.com/document/d/1PqASbYzS3898IQQHouvUMESJEFpr357xmpv6873T_2w/edit).
* Assign the sprint firefighter and discovery duty tasks to the new assignees.
* Ensure almost all tasks are ready and flagged as green (refined, assigned & with reviewer)
   before the meeting starts.

# Meeting agenda

* Start the meeting recording (try to start it early to capture all the conversations).
* Welcome any new team members and do a round of introductions if necessary.

**If it is the beginning of a new sprint:**

* Review past sprint, person by person.
    * Use each person's quick filter in JIRA, one at a time.
    * Quickly mention any major accomplishments, interesting tickets, lessons learned, or problems encountered that are worth sharing with the team.
* Review upcoming holidays / time off.
* Check any stories in the upcoming sprint which have a yellow marker:
    * It indicates missing assignments or estimation - solve these issues to get everything to green or purple, as well as any open question about the tickets.
    Let the person taking notes do the required changes in JIRA.
* Ensure the people on firefighting and discovery duty have extra tickets assigned:
    * They should be either in stretch goals or in the next sprint, in case there isn't enough discovery duty/firefighting work.
    * These tickets should have a green marker, i.e. be fully refined and have a reviewer assigned.
* Check for unassigned epics.
* Open the sprint retrospective spreadsheet to check that items have been
  entered. This is a last chance time for people in the meeting to add items
  to the spreadsheet.
* Go to the [Sprint Planning Dashboard](https://sprints.opencraft.com/) and end the sprint for your cell.
* Go to the [Spillovers Sheet](https://docs.google.com/spreadsheets/d/1aJD-e2NkDsyBq_yBHykGiMYE29FvSOuYSCZjOJ5sjkM) and review the uploaded spillovers with their reasons.

**If it is the middle of a sprint:**

* (If scheduled): tech talk or demos. See [all-hands meetings and tech talks](#all-hands-meetings-and-tech-talks) below.
* Ask if people anticipate not being able to finish some of their stories, or finishing early.  Reassign work to ensure everything gets finished in time.
* Review work assignments for people who have been off for the first week of the sprint.
* Review the last sprint's retrospective spreadsheet.
    * Everyone should cast 3 votes across the things to improve that they feel
      most important. For example, one could give 2 votes to item1 and 1 vote
      to item2.
    * Quickly discuss the top 3 items with the most votes and agree on a high
      level course of action. If follow up is required on an item, assign an
      owner to that item (the person who originally entered the item
      preferably) to manage further action such as opening tickets, beginning a
      forum discussion, etc.
    * Browse over the column of things that went well together.

**And always, to finish:**

* Announcements.
* Kudos.
* Open discussion. (Mid-sprint meetings only)

## All-hands meetings and tech talks

From time to time we get together and do tech talks, to share knowledge and as an opportunity to meet members of the other cells. These all-hands meetings follow a special procedure: On the day of a mid-sprint meeting, we all join the same meeting instead of a cell-specific meeting, then go through any important announcements and the tech talk together, and right after finishing we move on to our cell-specific mid-sprint meetings and continue as usual.

An all-hands meeting will be scheduled when there's something we want to discuss at the team level, or when there's a tech talk. To schedule a tech talk, create a ticket in your own cell to prepare it, then discuss in [the tech talks thread](https://forum.opencraft.com/t/all-hands-meetings-and-tech-talks/189/10) a date that works for all cells (it must be on the same day as a mid-sprint meeting).

The meeting room to use for the all-hands meeting can be the cell-specific room to which the person giving the tech talk belongs.

# After the meeting

If it's the beginning of a new sprint, the sprint manager from the cell that finishes the sprint planning meeting last, should complete the current sprint, start the next one, and create a new future sprint.
