# Free Software

## Running a Free Software project

An outside contributor is someone who proposes a change to a Free
Software repository for which OpenCraft is the maintainer. They may
fix or report a bug, propose or implement a new feature. OpenCraft
welcomes and encourages such contributions, even though they may be
unplanned and outside the roadmap. The [How to Run a Successful Free
Software Project](https://producingoss.com/) book covers everything
there is to know and this section is only about OpenCraft specifics:

* If, by chance, the contribution exactly matches an existing Xh
  ticket scheduled in the current sprint and not started, it
  essentially is an Xh gift to OpenCraft and is therefore worth Xh of
  review time. Because if it was not for the contribution an OpenCraft
  team member would have to do it and spend Xh anyways.

* If that is not the case, for each contribution a dedicated ticket
  timeboxed to 1h can be created in the relevant cell, under the
  Contributions Epic.

* If a firefight has time and is willing to, it is good to reply as
  soon as possible and schedule the ticket either to the upcoming
  sprint or to the current one if a firefighter has time, and inform
  the contributor of when to expect the review.
