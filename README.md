# public-doc

Public documentation repo for the OpenCraft handbook

See [handbook.opencraft.com](https://handbook.opencraft.com/) for the latest release.

This handbook is build using the static site generator [mkdocs](https://www.mkdocs.org/).

To build the docs to HTML, run from the root repository directory:

```bash
pip install -r requirements.txt
mkdocs build
```

To serve the docs locally, run from the root repository directory:

```bash
mkdocs serve
```

# Technical documentation

In February 2019 the technical documentation from this repository were
moved to a [dedicated
repository](https://gitlab.com/opencraft/documentation/tech).

# Read the Docs

* Configured with readthedocs.yml
* Account information [in Vault](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft Tools : Resources : Read the Docs)
* GitLab integration so the doc is rebuilt every time a commit is pushed in master is in the [settings of the public repository](https://gitlab.com/opencraft/documentation/public/settings/integrations)
